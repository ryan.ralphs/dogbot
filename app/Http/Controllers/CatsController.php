<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\CatService;
use App\Http\Controllers\BotManController;

class CatsController extends Controller
{
    public function __construct()
    {
        $this->photos = new CatService;
    }

    public function category($bot, $name)
    {
        $bot->reply($this->photos->category($name));
    }
}
