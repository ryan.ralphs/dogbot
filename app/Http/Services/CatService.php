<?php

namespace App\Http\Services;

use Exception;
use GuzzleHttp\Client;

class CatService
{
    // The endpoint we will be getting a random image from.
    const CATEGORY_ENDPOINT = 'https://thecatapi.com/api/images/get?format=xml&category=%s';

    /**
     * Guzzle client.
     *
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * CatService constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client;
    }

    /**
     * Fetch and return a random image from all breeds.
     *
     * @return string
     */
    public function random()
    {
        try {
            // Decode the json response.
            $response = json_decode(
            // Make an API call an return the response body.
                $this->client->get(self::RANDOM_ENDPOINT)->getBody()
            );

            // Return the image URL.
            return $response->message;
        } catch (Exception $e) {
            // If anything goes wrong, we will be sending the user this error message.
            return 'An unexpected error occurred. Please try again later.';
        }
    }

    /**
     * Fetch and return a random image from a given breed.
     *
     * @param string $category
     * @return string
     */
    public function category($category)
    {
        try {
            // We replace %s    in our endpoint with the given breed name.
            $endpoint = sprintf(self::CATEGORY_ENDPOINT, $category);
            $xml = simplexml_load_string($endpoint);
            $json = json_encode($xml);

            $response = json_decode(
                $this->client->get($json)->getBody()
            );

            return $response->message;
        } catch (Exception $e) {
            return "Sorry I couldn\"t get you any photos from $category. Please try with a different category.";
        }
    }


}